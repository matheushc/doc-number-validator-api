# Routes

### GET http://localhost:5000/all
##### response data
```shell
[
    {
        "docType": "0",
        "isBlocked": false,
        "number": "12345678909"
    }
]
```
- 200 - Success
___
### GET http://localhost:5000/validate/12345678909
##### response data
```shell
{
    "docType": 0
}
```
 - 200 - Success
 - 400 and {'error': 'Parameter is missing or blank'}

___
### POST http://localhost:5000/add
##### request data
```shell
{
    "number": "12345678909",
    "isBlocked": true
}
```
##### response data
- 200 - Success
- 400 and {'error': ''number' field is missing, blank or wrong type(string)'}
- 400 and {'error': 'isBlocked' field is missing or wrong type(boolean)'}
- 409 and {'error': 'Document Number already exists'}

___
### PUT http://localhost:5000/update
##### request data
```shell
{
    "number": "12345678909",
    "isBlocked": false
}
```
##### response data
- 400 and {'error': ''number' field is missing, blank or wrong type(string)'}
- 400 and {'error': ''isBlocked' field is missing or wrong type(boolean)'}
- 400 and {'error': 'Document Number is invalid'}
- 404 and {'error': 'Document Number not found'}
___
### DELETE http://localhost:5000/delete/12345678909
##### response data
- 400 and {'error': 'Parameter is missing or blank'}
- 400 and {'error': 'Document Number not found'}
___