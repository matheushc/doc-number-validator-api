FROM python:3.9.7
ADD . /doc-number-validator-api
WORKDIR /doc-number-validator-api
RUN pip install -r requirements.txt
EXPOSE 5000
CMD python main.py