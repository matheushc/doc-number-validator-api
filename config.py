from os import getenv


class RunConfig:

    MONGO_DBNAME = getenv('DOC_NUMBER_VALIDATOR_DATABASE_NAME')
    MONGO_USER = getenv('DOC_NUMBER_VALIDATOR_DATABASE_USER')
    MONGO_PASSWORD = getenv('DOC_NUMBER_VALIDATOR_DATABASE_PASSWORD')
    MONGO_ADDRESS = getenv('DOC_NUMBER_VALIDATOR_DATABASE_ADDRESS')
    MONGO_URI = f"mongodb://{MONGO_USER}:{MONGO_PASSWORD}@{MONGO_ADDRESS}/{MONGO_DBNAME}?authSource=admin"
    
class TestConfig:

    MONGO_URI = f"mongodb://localhost:27017/doc-number-validator-test"
    TESTING = True
