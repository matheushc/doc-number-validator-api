from src.validator.validator import validate_document_number, DocTypes

def test_validate_CPF():
    doc_nums = [
        {'num': '12345678910', 'type': DocTypes.UNKNOWN},
        {'num': '12345678909', 'type': DocTypes.CPF},
        {'num': '25634428777', 'type': DocTypes.CPF},
        {'num': 'AAAAAAAAAAA', 'type': DocTypes.UNKNOWN},
        {'num': 'A1A1A1A1A1A', 'type': DocTypes.UNKNOWN},
        {'num': '1111111111A', 'type': DocTypes.UNKNOWN},
        {'num': '1111111111AAAAAAAA', 'type': DocTypes.UNKNOWN},
    ]

    for cpf in doc_nums:
        assert validate_document_number(cpf['num']) == cpf['type']

def test_validate_CNPJ():
    doc_nums = [
        {'num': '82142574000169', 'type': DocTypes.CNPJ},
        {'num': '77659992000198', 'type': DocTypes.CNPJ},
        {'num': 'AAAAAAAAAAA123', 'type': DocTypes.UNKNOWN},
        {'num': 'A1A1A1A1A1A123', 'type': DocTypes.UNKNOWN},
        {'num': '111111111111AA', 'type': DocTypes.UNKNOWN},
        {'num': '22832191000190', 'type': DocTypes.CNPJ},
        {'num': '22832191000191AAAAAAA', 'type': DocTypes.UNKNOWN},
    ]

    for cpf in doc_nums:
        assert validate_document_number(cpf['num']) == cpf['type']

def test_has_just_one_algarism():
    for i in range(9):
        cpf = str(i) * 11
        status = validate_document_number(cpf)
        assert status == DocTypes.UNKNOWN

        cnpj = str(i) * 14
        status = validate_document_number(cnpj)
        assert status == DocTypes.UNKNOWN
