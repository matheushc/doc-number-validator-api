# import json
# from config import TestConfig
# from src.app.app import create_app

# def test_app():
#     app = create_app(TestConfig)
#     headers = {'content-type': 'application/json'}

#     with app.test_client() as c:
#         response = c.get('/all')
#         assert response.status_code == 200
#         # assert response.data == {} # pegando dados do db

#         response = c.get('/status')
#         assert response.status_code == 200

#         response = c.get('/validate/12345678909')
#         assert response.status_code == 200
#         assert response.data == b'{"docType":0}\n'

#         response = c.get('/validate/22832191000190')
#         assert response.status_code == 200
#         assert response.data == b'{"docType":1}\n'

#         response = c.get('/validate/12345678910')
#         assert response.status_code == 200
#         assert response.data == b'{"docType":2}\n'

#         updateData = {'number': '12345678909', 'isBlocked': True}
#         response = c.put('/update', data=json.dumps(updateData), headers=headers)
#         assert response.status_code == 200

#         # insertData = {'number': '12345678910', 'isBlocked': False}
#         # response = c.post('/add', data=json.dumps(insertData), headers=headers)
#         # assert response.status_code == 200
