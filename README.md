# doc-number-validator-api

## Description
A project for validate and save brazilian's document numbers (CPF and CNPJ).

- Rest API:
   - Python (Flask )
   - Database: MongoDB
- Web:
   - ReactJS

## Depedencies
 - Make
 - Docker
 - Git

## Run project
```shell
make docker-up
```

## Run tests
```shell
make test
```

## Project Page
http://localhost:3000/

## Web project
https://gitlab.com/matheushc/doc-number-validator-web

## API documentation
[Api doc](doc/api.md)

## Improvement Points
 - API Tests
    - Routes
    - Mock Database
 - Web Tests

## Document numbers validation source
https://souforce.cloud/regra-de-validacao-para-cpf-e-cnpj/