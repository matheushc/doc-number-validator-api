.PHONY: test

test:
	@pytest

docker-up:
	@git submodule update --init
	@docker-compose up --build

docker-down:
	@docker-compose down