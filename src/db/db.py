from src.resource.document_number import DocumentNumber 

def get_all(cursor):
    output = []
    for dn in cursor.find():
        output.append(DocumentNumber(
            dn['number'], dn['docType'], dn['isBlocked']).to_json())
    return output

def find(cursor, number):
    return cursor.find_one({'number': number})

def insert(cursor, data):
    status = cursor.insert_one(data)
    if not status.inserted_id:
        return False
    return True

def update(cursor, data):
    status = cursor.update_one({'number': data['number']}, {"$set": data})
    if not status.matched_count:
        return False
    return True

def delete(cursor, number):
    status = cursor.delete_one({'number': number})
    if not status.deleted_count:
        return False
    return True
