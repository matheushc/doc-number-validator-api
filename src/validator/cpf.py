from .validator import DocTypes

def validate_cpf(number):
    if not _is_valid_digit(number):
        return DocTypes.UNKNOWN
    return DocTypes.CPF

def _is_valid_digit(number):
    for digit_position in [10, 11]:
        results = []
        mult_start = digit_position
        for n in number[:digit_position - 1]:
            results.append(int(n) * mult_start)
            mult_start -= 1
        result = 11 - (sum(results) % 11)
        digit = 0 if result > 9 else result
        if digit != int(number[digit_position - 1]):
            return False
    return True