from src.resource.document_number import DocTypes
from .cpf import validate_cpf
from .cnpj import validate_cnpj

def get_doc_type(number):
    if len(number) == 11:
        return DocTypes.CPF
    elif len(number) == 14:
        return DocTypes.CNPJ
    return DocTypes.UNKNOWN

def validate_document_number(number):
    if not number.isdigit():
        return DocTypes.UNKNOWN

    if _has_just_one_algarism(number):
        return DocTypes.UNKNOWN

    doc_type = get_doc_type(number)
    if doc_type == DocTypes.CPF:
        return validate_cpf(number)
    elif doc_type == DocTypes.CNPJ:
        return validate_cnpj(number)
    return DocTypes.UNKNOWN

def _has_just_one_algarism(number):
    for n in number[1:]:
        if n != number[0]:
            return False
    return True
