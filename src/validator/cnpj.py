from .validator import DocTypes

multipliers = [6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2]

def validate_cnpj(number):
    if not _is_valid_digit(number):
        return DocTypes.UNKNOWN
    return DocTypes.CNPJ

def _is_valid_digit(number):
    for digit_position in [13, 14]:
        results = []
        multi = multipliers if digit_position == 14 else multipliers[1:]
        for i, m in enumerate(multi):
            results.append(int(number[i]) * m)
        result = sum(results) % 11
        digit = 0 if result < 2 else 11 - result
        if digit != int(number[digit_position - 1]):
            return False
    return True
