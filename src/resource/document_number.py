from enum import IntEnum
from json import dumps

class DocTypes(IntEnum):
    CPF = 0
    CNPJ = 1
    UNKNOWN = 2

class DocumentNumber():
    def __init__(self, number, doc_type=DocTypes.CPF, is_blocked=False):
        self.number = number
        self.doc_type = dumps(doc_type) if isinstance(doc_type, IntEnum) else doc_type
        self.is_blocked = is_blocked

    def to_json(self):
        return {
            'number': self.number,
            'docType': self.doc_type,
            'isBlocked': self.is_blocked
        }
