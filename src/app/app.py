from flask import Flask, jsonify, request
from flask_cors import CORS
from dotenv import load_dotenv
from os import getenv
from flask_pymongo import PyMongo
from src.resource.document_number import DocTypes, DocumentNumber
from src.validator.validator import validate_document_number
from src.db import db
from time import time

start_time = time()
req_quantity = 0
cursor = None
load_dotenv()

def create_app(config):
    app = Flask(__name__)
    cors = CORS(app)

    app.config.from_object(config)

    connect_db(app)

    @app.route('/all', methods=['GET'])
    def get_all_numbers():
        output = db.get_all(cursor)
        return jsonify(output)

    @app.route('/validate/<number>', methods=['GET'])
    def validate_number(number):
        if not number.strip():
            return {'error': 'Parameter is missing or blank'}, 400
        global req_quantity
        req_quantity += 1
        return jsonify({'docType': validate_document_number(number)})

    @app.route('/add', methods=['POST'])
    def add_number():
        if 'number' not in request.json or not isinstance(request.json['number'], str) or \
                not request.json['number'].strip():
            return {'error': '\'number\' field is missing, blank or wrong type(string)'}, 400

        if 'isBlocked' not in request.json or not isinstance(request.json['isBlocked'], bool):
            return {'error': '\'isBlocked\' field is missing or wrong type(boolean)'}, 400

        number = request.json['number']
        if db.find(cursor, number):
            return {'error': 'Document Number already exists'}, 409

        docType = validate_document_number(number)
        if docType == DocTypes.UNKNOWN:
            return {'error': 'Document Number is invalid'}, 400

        dn = DocumentNumber(number, docType, request.json['isBlocked']).to_json()
        if not db.insert(cursor, dn):
            return {'error': 'Something went wrong'}, 400

        return ''

    @app.route('/update', methods=['PUT'])
    def update_number():
        if 'number' not in request.json or not isinstance(request.json['number'], str) or \
                not request.json['number'].strip():
            return {'error': '\'number\' field is missing, blank or wrong type(string)'}, 400

        if 'isBlocked' not in request.json or not isinstance(request.json['isBlocked'], bool):
            return {'error': '\'isBlocked\' field is missing or wrong type(boolean)'}, 400

        number = request.json['number']
        docType = validate_document_number(number)
        if docType == DocTypes.UNKNOWN:
            return {'error': 'Document Number is invalid'}, 400

        dn = DocumentNumber(number, docType, request.json['isBlocked']).to_json()
        if not db.update(cursor, dn):
            return {'error': 'Document Number not found'}, 404

        return ''

    @app.route('/delete/<number>', methods=['DELETE'])
    def delete_number(number):
        if not number.strip():
            return {'error': 'Parameter is missing or blank'}, 400

        status = db.delete(cursor, number)
        if not status:
            return {'error': 'Document Number not found'}, 400

        return ''

    @app.route('/status', methods=['GET'])
    def get_status():
        return jsonify({
            "uptime": get_uptime(),
            "reqQuantity": req_quantity
        })

    return app

def connect_db(app):
    mongo = PyMongo(app)
    global cursor
    cursor = mongo.db['doc-numbers']

def get_uptime():
    global start_time
    return time() - start_time
