from config import RunConfig
from src.app.app import create_app
from os import getenv

if __name__ == '__main__':
    app = create_app(RunConfig)
    app.run(debug=True,
        host=getenv('DOC_NUMBER_VALIDATOR_HTTP_HOST'),
        port=getenv('DOC_NUMBER_VALIDATOR_HTTP_PORT'))
